import '../styles/globals.css'
import { useState, useEffect } from 'react';
import Head from 'next';
import { Form, Button } from 'react-bootstrap'

function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}

export default MyApp
